#!/usr/bin/perl
use strict;
use warnings;
use DBI;
use LWP::UserAgent;
use XML::Simple;
use Sys::Syslog qw(:standard :macros);  
use File::Basename;
use Getopt::Long;
use Pod::Usage;
use Socket;
use Data::Dumper;


my $DBUSER='root';
my $DBPASS=undef;
my $DBNAME='powerdns';
my $DBHOST='localhost';
my $DBPORT='3306';

my $AUTODNS_CONTEXT=1;
my $AUTODNS_USER=undef;
my $AUTODNS_PASS=undef;
#my $AUTODNS_URL='https://gateway.autodns.com';
my $AUTODNS_URL='https://demo.autodns.com/gateway/';

my $PRIMARY_NAMESERVER='';
my $PRIMARY_IPS='';
my $SOURCE_IP='';
my $LIMIT='10';
my $NSACTION='secondary';
my $HELP=undef;
my $DEBUG=undef;

GetOptions (
	"help|?|h" => \$HELP,
	"dbuser=s" => \$DBUSER,
	"dbpass=s" => \$DBPASS,
	"dbname=s" => \$DBNAME,
	"dbhost=s" => \$DBHOST,
	"dbport=s" => \$DBPORT,
	"autodnsuser=s" => \$AUTODNS_USER,
	"autodnspass=s" => \$AUTODNS_PASS,
	"autodnscontext=s" => \$AUTODNS_CONTEXT,
	"autodnsurl=s" => \$AUTODNS_URL,
	"nsaction=s" => \$NSACTION,
	"primary=s" => \$PRIMARY_NAMESERVER,
	"primaryips=s" => \$PRIMARY_IPS,
	"sourceip=s" => \$SOURCE_IP,
	"limit=i" => \$LIMIT,
	"debug" => \$DEBUG,
); 

if ($HELP){
	pod2usage(1);
}


my $dbh = DBI->connect(
            "DBI:mysql:$DBNAME:$DBHOST:$DBPORT",
            $DBUSER,$DBPASS
        ) or die "Can't connect to $DBNAME on $DBHOST\n";


sub logmsg {
	my $prio=shift||LOG_INFO;
	my $message=shift||'';
	my $script=basename($0);
	print STDERR "$message\n" if $DEBUG;
	syslog($prio,$script."[".$$."]: ".$message);
}

sub parse_request {
	my $xml=shift;
	my $xs = new XML::Simple(forcearray => 1, keeproot => 0);;
	my $ref = $xs->XMLin($xml);
	print Dumper($ref) if $DEBUG;

	my $status= $ref 
		&& exists $ref->{'result'} 
		&& exists $ref->{'result'}->[0] 
		&& exists $ref->{'result'}->[0]->{'status'}
		&& exists $ref->{'result'}->[0]->{'status'}->[0]
		&& exists $ref->{'result'}->[0]->{'status'}->[0]->{'type'}
		&& exists $ref->{'result'}->[0]->{'status'}->[0]->{'type'}->[0]
		? $ref->{'result'}->[0]->{'status'}->[0]->{'type'}->[0] : undef;

	my $error= $ref 
		&& exists $ref->{'result'} 
		&& exists $ref->{'result'}->[0] 
		&& exists $ref->{'result'}->[0]->{'msg'}
		&& exists $ref->{'result'}->[0]->{'msg'}->[0]
		&& exists $ref->{'result'}->[0]->{'msg'}->[0]->{'code'}
		&& exists $ref->{'result'}->[0]->{'msg'}->[0]->{'code'}->[0]
		? $ref->{'result'}->[0]->{'msg'}->[0]->{'code'}->[0] : undef;
	
	my $error_msg= $ref
		&& exists $ref->{'result'} 
		&& exists $ref->{'result'}->[0] 
		&& exists $ref->{'result'}->[0]->{'msg'}
		&& exists $ref->{'result'}->[0]->{'msg'}->[0]
		&& exists $ref->{'result'}->[0]->{'msg'}->[0]->{'text'}
		&& exists $ref->{'result'}->[0]->{'msg'}->[0]->{'text'}->[0]
		? $ref->{'result'}->[0]->{'msg'}->[0]->{'text'}->[0] : undef;

	my $data= $ref
		&& exists $ref->{'result'} 
		&& exists $ref->{'result'}->[0] 
		&& exists $ref->{'result'}->[0]->{'data'}
		? $ref->{'result'}->[0]->{'data'} : undef;

	logmsg($status eq 'error' ? LOG_ERR : LOG_INFO , "Request status $status: ".($error_msg||''));

	return ($status,$error,$data);
}

sub send_request {
	my $xml=shift;
	my $ua = new LWP::UserAgent;
	$ua->agent("autodns-tools; $0/0.1; mschrieck;" . $ua->agent);

	my $req = new HTTP::Request POST => $AUTODNS_URL;

	$ua->timeout(600);
	$req->content_type('text/xml');
	$req->content($xml);
	my $res = $ua->request($req);
	
	if (!$res->is_success) {
		logmsg(LOG_ERR,"Cant send to $AUTODNS_URL: ".$res->status_line);
		return;
	}
	logmsg(LOG_INFO,"Send request to $AUTODNS_URL .".$res->code);
	print "request:\n$xml\n----\n" if $DEBUG;
	print "response:\n".$res->decoded_content."\n----\n" if $DEBUG;
	return $res->decoded_content;
}

sub set_status {
	my $id=shift;
	my $status_new=shift;
	my $status_old=shift;
	my $error=shift || '';

	chomp($error);
	my $sql="update domain_events set status=?,error_code=? where status=? and id=?" ;
	my $sth=$dbh->prepare($sql);
	$sth->execute($status_new,$error,$status_old,$id);
	if ($sth->rows ){
		return 1;
	}
	return 0;
}

sub get_ns_records {
	my $name=shift;
	my $sql="select lower(content) content from records r left join domains d on (r.domain_id=d.id) where d.name=? and r.type='NS' and r.name=d.name;" ;
	my $sth=$dbh->prepare($sql);
	$sth->execute($name);
	
	my @nserver=();
	while ( my $row=$sth->fetchrow_hashref() ) {
		my $ns=$row->{content};
		$ns=~s/\.$//;
		# skip primary NS
		push @nserver,$row->{content};
	}
	return \@nserver;
}

# sort primary on first place
sub sort_ns {
	my $ns=shift;
	my $primary=shift;
	my @nserver=($primary);

	# primary must be the first NS
	foreach ( @$ns ) {
		# skip primary NS
		next if ( $_ eq $primary );
		push @nserver,$_;
	}
	return \@nserver;
}

# since v1.1 we dont know the primary nameserver
# if we got a delete event
# so we need to inquire the zones before and
# search for zones where we are the primary
sub autodns_zone_list {
	my $name=shift;
	my $xml=qq(
	<request>
		<auth>
			<user>$AUTODNS_USER</user>
			<password>$AUTODNS_PASS</password>
			<context>$AUTODNS_CONTEXT</context>
		</auth>
		<task>
			<code>0205</code>
			<view>
				<children>1</children>
			</view>
			<where>
				<key>name</key>
				<operator>eq</operator>
				<value>$name</value>
			</where>
		</task>
	</request>
	);
	my $res=send_request($xml);
	return parse_request($res);
}

sub autodns_zone_import {
	my $name=shift;
	my $ns=shift;
	my $nsaction=shift;
	my $source=shift;
	my $nserver_tmpl=get_ns_template_string($ns);
	
	my $xml=qq(
	<request>
		<auth>
			<user>$AUTODNS_USER</user>
			<password>$AUTODNS_PASS</password>
			<context>$AUTODNS_CONTEXT</context>
		</auth>
		<task>
			<code>0204</code>
			<zone>
				<source>$source</source>
				<name>$name</name>
				<ns_action>$nsaction</ns_action>
                                <www_include>0</www_include>
				$nserver_tmpl
			</zone>
		</task>
	</request>
	);
	my $res=send_request($xml);
	return parse_request($res);
}

# create slave zone in autodns 
# so mode can only be secondary or hidden
sub autodns_zone_create {
	my $name=shift;
	my $ns=shift;
	my $nsaction=shift;
	my $nserver_tmpl=get_ns_template_string($ns);
	
	my $xml=qq(
	<request>
		<auth>
			<user>$AUTODNS_USER</user>
			<password>$AUTODNS_PASS</password>
			<context>$AUTODNS_CONTEXT</context>
		</auth>
		<task>
			<code>0201</code>
			<zone>
				<name>$name</name>
				<ns_action>$nsaction</ns_action>
				$nserver_tmpl
			</zone>
		</task>
	</request>
	);
	my $res=send_request($xml);
	return parse_request($res);
}

# create slave zone in autodns 
# so mode can only be secondary or hidden
sub autodns_zone_delete {
	my $name=shift;
	my $ns=shift;

	# system NS is first ns
	my $system_ns=shift @$ns;
	
	my $xml=qq(
	<request>
		<auth>
			<user>$AUTODNS_USER</user>
			<password>$AUTODNS_PASS</password>
			<context>$AUTODNS_CONTEXT</context>
		</auth>
		<task>
			<code>0203</code>
			<zone>
				<name>$name</name>
				<system_ns>$system_ns</system_ns>
			</zone>
		</task>
	</request>
	);
	my $res=send_request($xml);
	return parse_request($res);
}

#
# filter out the primary nameserver from a list of ns by resolving 
# the ip and compare it with a given list
#
sub get_primary_from_list_and_ips {
	my $ns_list=shift;
	my $local_ips=shift;
	print STDERR "try to find primary NS:".join(',',@$ns_list)." IP:".join(',',@$local_ips) ."\n" if $DEBUG;
	foreach my $name (@$ns_list) {
		my $ips=resolve($name);
		foreach my $ip (@$ips ) {
			if (grep $ip eq $_ , @$local_ips ){
				return $name;
			}
		}
	}
	die "cant find primary ns\n";
}


sub resolve {
	my $name=shift;
	my @addr=();

	@addr = gethostbyname($name) or die "Can't resolve $name: $!\n";
	@addr = map { inet_ntoa($_) } @addr[4 .. $#addr];
	return \@addr;
}

sub action_import{
	my $name=shift;
	my $ns=shift;
	my $nsaction=shift;
	my $source=shift;
	my ($status,$error,$data)=autodns_zone_import($name,$ns,$nsaction,$source);
	# expected errors
	# zone already exists because autodns also insert this event again 
	if ($status eq 'error' and $error eq 'EF02019'){
		$status='success';
		$error='';
	}
	return ($status,$error,$data);
}
sub action_create {
	my $name=shift;
	my $nsaction=shift;
	my $ns=shift;
	my ($status,$error,$data)=autodns_zone_create($name,$ns,$nsaction);
	if ($status eq 'error' and $error eq 'EF02019'){
		$status='success';
		$error='';
	}
	return ($status,$error,$data);
}

sub action_delete {
	my $name=shift;
	my $ns=shift;
	my ($status,$error,$data)=autodns_zone_delete($name,$ns);
	# expected errors
	# zone does not because autodns triggert the event (domain is deleted in autodns)
	if ($status eq 'error' and $error eq 'EF02020'){
		$status='success';
		$error='';
	}
	return ($status,$error,$data);
}


# 
# get all system ns from zone list inquire
#
sub get_system_ns_from_list {
	my $data=shift;
	my $zone_count=0;
	my $ns=[];
	if (ref $data 
		&& exists $data->[0] 
		&& exists $data->[0]->{'summary'}
		&& exists $data->[0]->{'summary'}->[0]	
	) {
		$zone_count=$data->[0]->{'summary'}->[0];
	}

	if ($zone_count > 0 ) {
	
		foreach my $entry ( @{ $data } ) {
			foreach my $zone ( @{ $entry->{'zone'}} ){
				push @$ns,$zone->{'system_ns'}->[0];
			}
		};
	}
	return $ns;
}

my $sql="select * from domain_events where status='pending' limit $LIMIT" ;
my $sth=$dbh->prepare($sql);
$sth->execute();
while ( my $row=$sth->fetchrow_hashref() ) {
	my $id=$row->{'id'};
	my $name=$row->{'name'};
	my $action=$row->{'action'};
	my ($status,$error,$data)=();
	my @primaryips=split /,/, $PRIMARY_IPS;

	# skip entry if status cant be set maybe parallel script execution
	next unless set_status($id,'processing','pending');

	eval {

		if ($action eq 'create' and grep $NSACTION eq $_ , qw( hidden secondary ) ) {
			my $ns=get_ns_records($name);

			if ($PRIMARY_NAMESERVER) { 
				$ns=sort_ns($ns,$PRIMARY_NAMESERVER);

			} else {
				# resolve primary
				my $primary=get_primary_from_list_and_ips($ns,\@primaryips);
				$ns=sort_ns($ns,$primary);
			}
			($status,$error,$data)=action_create($name,$ns,$NSACTION);

		} elsif ($action eq 'create' and grep $NSACTION eq $_ , qw( complete primary )) {
			my $ns=get_ns_records($name);

			if ($PRIMARY_NAMESERVER) { 
				$ns=sort_ns($ns,$PRIMARY_NAMESERVER);

			} else {
				# resolve primary
				my $primary=get_primary_from_list_and_ips($ns,\@primaryips);
				$ns=sort_ns($ns,$primary);
			}
			($status,$error,$data)=action_import($name,$ns,$NSACTION,$SOURCE_IP);
		} elsif ($action eq 'delete') {
			my $ns=[];

			if ($PRIMARY_NAMESERVER) { 
				$ns=[$PRIMARY_NAMESERVER];
				($status,$error,$data)=action_delete($name,$ns);

			} else {
				# GET NS from AutoDNS

				my $zone_count=0;
				($status,$error,$data)=autodns_zone_list($name);
				$ns=get_system_ns_from_list($data);

				# resolve primary
				my $primary=get_primary_from_list_and_ips($ns,\@primaryips);
				$ns=sort_ns($ns,$primary);
				($status,$error,$data)=action_delete($name,$ns);

			}
		}
		else {
			die "Unknown action in event table\n";
		}
	};
	if (my $err=$@) { 
		($status,$error)=('error',$err);
	};
	
	logmsg($status eq 'error' ? LOG_ERR : LOG_INFO ,  uc($status).": $action $name ".($error ||'' )."\n");

	set_status($id,$status,'processing',$error);
}

sub get_ns_template_string {
	my $ns=shift;
	my $tmpl="\n";
	foreach ( @$ns ) {
		$tmpl.="<nserver><name>$_</name></nserver>\n";
	}
	return $tmpl;
}


__END__

=head1 NAME

powerdns-master-sync.pl - sync new/deleted zones to autodns

=head1 SYNOPSIS

powerdns-master-sync.pl [options]

=head1 OPTIONS

=over 8

=item B<-help>
Print a brief help message and exits.

=item B<-dbuser>
Database User

=item B<-dbpass>
Database Password

=item B<-dbhost>
Database Host

=item B<-dbport>
Database Port

=item B<-dbname>
Database Name

=item B<-autodnsuser>
AutoDNS User

=item B<-autodnspass>
AutoDNS Password

=item B<-autodnscontext>
AutoDNS Context

=item B<-autodnsurl>
AutoDNS URL

use https://gateway.autodns.com/ for LIVE System
or
use https://demo.autodns.com/gateway/ for TEST System


=item B<-primary>
define Primary Nameserver by name

=item B<-primaryips>
Primary NS is resolved by ip and compared with this ips

=item B<-sourceip>
sourceip for imports

=item B<-limit>
maximum events to process

=back

=head1 DESCRIPTION

B<powerdns-master-sync.pl> will read added or delete zones from a

Table and sends the correspending AutoDNS Task. So the the zone 
is created/delete on the secondary NS from AutoDNS.

L<http://mschrieck.blogspot.com/2014/05/using-autodns-as-powerdns-slave-server.html>

=cut

